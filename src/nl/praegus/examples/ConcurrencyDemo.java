public class ConcurrencyDemo implements Runnable {
    private static final int NR_OF_THREADS = 3;
    
    private int mThreadNr;
    
    public static void main (String[] args) {
        for (int threadNr = 0; threadNr < NR_OF_THREADS; threadNr++) {
            System.out.println ("x " + threadNr);
            Thread thread = new Thread (new ConcurrencyDemo (threadNr));
            thread.start ();
        }
    }

    private ConcurrencyDemo (int threadNr) {
        mThreadNr = threadNr;
    }
    
    @Override
    public void run () {
        while (true) {
            System.out.println (mThreadNr);
            waitAMoment ();
        }
    }

    private void waitAMoment () {
        try {
            Thread.sleep ((long) (Math.random () * 1000.0));
        } catch (InterruptedException ex) {
            // ignore
        }
    }
}
