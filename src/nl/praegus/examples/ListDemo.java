package nl.praegus.examples;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListDemo {

    public static void main(String[] args) {
        List<Integer> arrayList = new ArrayList<>();
        fillList(arrayList);
        System.out.println(arrayList);

        System.out.println("Let's iterate!");

        //Iterator demo
        Iterator<Integer> iter = arrayList.iterator();
        while (iter.hasNext()) {
            Integer value = iter.next();
            System.out.println(value);
        }

        /* Can be replaced by:
        for (Integer value : arrayList) {
            System.out.println(value);
        }
         */
    }

    private static void fillList(List<Integer> list) {
        for(int i = 0; i <= 25; i++) {
            list.add(i);
        }
    }

}
