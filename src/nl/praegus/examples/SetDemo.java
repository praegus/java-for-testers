package nl.praegus.examples;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {
    public static void main (String[] args) {
        Set<String> workshops = new HashSet<> ();
        checkForEmpty (workshops);

        workshops.add ("Java4Testers");
        checkForEmpty (workshops);

        workshops.add ("Knitting");
        workshops.add ("Whistling");
        workshops.add ("Knitting");
        workshops.add ("Java4Testers");
        workshops.add ("Knitting");
        checkForEmpty (workshops);

        workshops.remove ("Knitting");
        checkForEmpty (workshops);

        workshops.clear ();
        checkForEmpty (workshops);
    }
    
    static void checkForEmpty (Set<String> set) {
        if (set.isEmpty ()) {
            System.out.println ("set is empty");
        } else {
            System.out.println ("set is not empty");
            System.out.println ("set contains " + set.size () + " items:");
            Iterator<String> iter = set.iterator ();
            while (iter.hasNext ()) {
                String item = iter.next ();
                System.out.println ("- " + item);
            }
        }
        System.out.println ();
    }
}
