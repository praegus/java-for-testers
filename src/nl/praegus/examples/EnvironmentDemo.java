package nl.praegus.examples;

import java.util.Map;

public class EnvironmentDemo {
    public static void main (String[] args) {
        Map<String, String> envVariables = System.getenv ();
        for (String name : envVariables.keySet ()) {
            System.out.println (name + ": " + envVariables.get (name));
        }
    }
}
