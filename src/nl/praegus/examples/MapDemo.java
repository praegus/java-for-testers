package nl.praegus.examples;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MapDemo {
    public static void main (String[] args) {
        Map<String, String> workshops = new HashMap<> ();
        write (workshops);
        
        workshops.put ("Tom", "Java4Testers");
        workshops.put ("Dick", "Agile Testing");
        workshops.put ("Harry", "Specification by Example");
        write (workshops);
        
        workshops.put ("Dick", "Behavior Driven Development");
        workshops.put ("Martin", "Knitting");
        write (workshops);
        
        workshops.remove ("Dick");
        write (workshops);

        workshops.clear ();
        write (workshops);
    }
    
    static void write (Map<String, String> workshops) {
        if (workshops.isEmpty ()) {
            System.out.println ("map is empty");
        } else {
            for (String person : workshops.keySet ()) {
                System.out.println (person + ": " + workshops.get (person));
            }
        }
        System.out.println ();
    }
}
