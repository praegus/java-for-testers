package nl.praegus.examples.refactoring;


public interface ProcedureRunner {
    void invokeSource (TestSource source);
    Scope getCurrentScope ();
}
