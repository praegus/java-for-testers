package nl.praegus.examples.refactoring;


/**
 * The Engine runs a test by repeating three steps:
 * 1) get a test line,
 * 2) evaluate its expressions,
 * 3) execute it.
 * <BR/><BR/>
 * The engine obtains test lines from the test line source(s), one by one.
 * If a test source finds a procedure (i.e. a scripted instruction),
 * the engine adds it to the set of known procedures.
 * <BR/><BR/>
 * The Instructions class provides the executor for known instructions.
 * <BR/><BR/>
 * A test can run to completion or be aborted.
 * <BR/><BR/>
 * Most of the test state is contained in the RunTime,
 * so that it can be accessed by (user defined) instruction sets.
 */
public class testEngine {
    RunTimeImpl runtime;
    TestRunResultPublisher publisher;
    TestSourceStack source_stack;
    TestLine test_line;
    Instructions instructions;
    boolean isaborting;
    

    void abort () {
        isaborting = true;
        runtime.mSourceStack.abortTest ();
    }

    
    testEngine (RunTimeImpl runTime) {
        runtime      = runTime;
        publisher    = runTime.getPublisher ();
        source_stack = runTime.getSourceStack ();
        instructions = new Instructions (runTime, publisher);
        isaborting   = false;
    }


    void run (TestSource source) {
        publisher.start (null);
            runtime.invokeSource (source);
            run ();
        instructions.cleanup ();
        publisher.finish ();
    }

    void run () {
        while (get_test_line ()) {
            if (evaluateTestLine ()) {
                if (!test_line.getPart (0).isEmpty ()) {
                    try {
                        publisher.publishTestLine (test_line);
                        Executor executor = instructions.getExecutor (test_line.getPart (0));
                        if (!executor.execute (test_line)) {
                            publisher.publishError ("instruction returned 'false'");
                        }
                    } catch (ExecutionException ee) {
                        publisher.publishError (ee.getMessage ());
                        if (ee.hasStackTrace ()) {
                            publisher.publishStackTrace (ee);
                        }
                    } catch (Exception e) {
                        publisher.publishError (e.toString () + " caught: " + e.getMessage ());
                        if (e.getStackTrace () != null) {
                            publisher.publishStackTrace (e);
                        }
                    }
                    if (isaborting) {
                        publisher.publishWarning ("aborting run");
                    }
                    publisher.publishEndOfTestLine ();
                } else {
                    if (!test_line.isEmpty ()) {
                        publisher.publishCommentLine (test_line);
                    }
                }
            }
        }
        publisher.publishEndOfSection ();
    }

    boolean get_test_line () {
        while (true)
        {
            try {
                
            test_line = runtime.mSourceStack.getTestLine ();
            return test_line != null;
            }
            catch (ProcedureException pe)
            {
                if (pe.getProcedure()!=null) instructions.addProcedure (pe.getProcedure ());
            }
        }
    }

    boolean evaluateTestLine () {
        try {
            runtime.evaluateExpressions (test_line);
            return true;
        } catch (ExecutionException ee) {
            publisher.publishTestLine (test_line);
            publisher.publishError (ee.getMessage ());
            if (ee.hasStackTrace ()) {
                publisher.publishStackTrace (ee);
            }
            publisher.publishEndOfTestLine ();
        } catch (Exception e) {
            publisher.publishTestLine (test_line);
            publisher.publishError (e.toString()+" caught: "+e.getMessage());
            if (e.getStackTrace () != null) {
                publisher.publishStackTrace (e);
            }
            publisher.publishEndOfTestLine ();
        }
        return false;
    }
}
