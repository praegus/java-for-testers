package nl.praegus.examples.refactoring;

import java.util.Date;


public interface TestRunResultPublisher {
    void start (Date dateTime);
    void finish ();
    void reset ();

    // test case
    void publishTestCaseBegin (String id, String description);
    void publishTestCaseEnd ();

    void publishTestLine (TestLine testLine);
    void publishCommentLine (String commentLine);
    void publishCommentLine (TestLine commentLine);
    void publishIncreaseLevel ();
    void publishDecreaseLevel ();
    void publishEndOfSection ();
    void publishEndOfTestLine ();

    // results
    void publishError (String message);
    void publishStackTrace (Exception e);
    void publishValueError (String expression, String actualValue, String expectedValue);
    void publishWarning (String message);
    void publishInfo (String message);
    void publishLink (String url);
    void publishValue (String expression, String value);

    // model events
    void publishNewState (String name);
    void publishNewTransition (String sourceName, String targetName);
    void publishAtState (String name);
    void publishAtTransition (String sourceName, String targetName);
}
