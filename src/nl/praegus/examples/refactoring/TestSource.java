package nl.praegus.examples.refactoring;


/*
 * A TestSource contains instructions and provides them one by one.
 */
public interface TestSource {
    void initialize ();
    void cleanup ();

    boolean    isATestCase ();
    TestLine   getTestLine ();
    Scope      getScope ();

    TestSource create (String sourceName);
    TestSource createTestCase (String name, String description);
}
