/* Copyright 2012-2014 by Martin Gijsen (www.DeAnalist.nl)
 *
 * This file is part of the PowerTools engine.
 *
 * The PowerTools engine is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The PowerTools engine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with the PowerTools engine. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.praegus.examples.refactoring;

import java.util.HashMap;
import java.util.Map;


/**
 * The runtime provides a full API for instructions (and engine logic) to access
 * 1) the test source stack, 2) reporting, 3) roles and 4) shared objects.
 * <BR/><BR/>
 * The test source stack is used for creating and finding symbols.
 * <BR/><BR/>
 * Reporting concerns errors, warnings and info messages.
 * <BR/><BR/>
 * Roles represent combinations of user names and passwords in a test environment.
 * <BR/><BR/>
 * Shared objects are used to exchange information between instruction sets.
 */
public final class RunTimeImpl implements RunTime, ProcedureRunner {
    final TestSourceStack mSourceStack;

    private final TestRunResultPublisher mPublisher;
    private final Map<String, Object>    mSharedObjects;


    public RunTimeImpl () {
        mSourceStack         = null;
        mPublisher           = null;
        mSharedObjects       = new HashMap<String, Object> ();
    }


    @Override
    public TestSourceStack getSourceStack () {
        return mSourceStack;
    }

    @Override
    public boolean addSharedObject (String name, Object object) {
        if (mSharedObjects.containsKey (name)) {
            return false;
        } else {
            mSharedObjects.put (name, object);
            return true;
        }
    }

    @Override
    public Object getSharedObject (String name) {
        if (mSharedObjects.containsKey (name)) {
            return mSharedObjects.get (name);
        } else {
            return null;
        }
    }


    @Override
    public TestRunResultPublisher getPublisher () {
        return mPublisher;
    }


    @Override
    public Scope getCurrentScope () {
        return mSourceStack.getCurrentScope ();
    }

    @Override
    public void invokeSource (TestSource source) {
        mSourceStack.initAndPush (source);
    }

    @Override
    public String evaluateExpression (String expression) {
        return "";
    }

    void evaluateExpressions (TestLine originalTestLine) {
        // ...
    }
}
