package nl.praegus.examples.refactoring;

import java.util.HashMap;
import java.util.Map;


/**
 * The Instructions contains all known instructions,
 * both scripted ones (i.e. procedures) and programmed ones.
 * <BR/><BR/>
 * An instruction set for procedures is already available.
 * Any number of instruction sets can be added.
 * <BR/><BR/>
 * An instruction name to search for can include the instruction set name.
 * An executor cache ensures that an instruction only has to be looked up once.
 * <BR/><BR/>
 * It is emptied when an instruction set is added to avoid name conflicts.
 */
final class Instructions {
    private final Map<String, InstructionSet> mInstructionSets;
    private final Map<String, Executor> mExecutorCache;
    private final Procedures mProcedures;


    Instructions (ProcedureRunner runner, TestRunResultPublisher publisher) {
        mInstructionSets = new HashMap<String, InstructionSet> ();
        mExecutorCache   = new HashMap<String, Executor> ();
        mProcedures      = new Procedures (runner, publisher);
        addInstructionSet (mProcedures);
    }

    void addInstructionSet (InstructionSet set) {
        String name = set.getName ();
        if (!mInstructionSets.containsKey (name)) {
            mInstructionSets.put (name, set);
            set.setup ();
            mExecutorCache.clear ();
        } else {
            throw new ExecutionException ("an instruction set '%s' is already registered", name);
        }
    }

    void addProcedure (Procedure procedure) {
        mProcedures.add (procedure);
    }

    Executor getExecutor (String instructionName) {
        Executor executor = mExecutorCache.get (instructionName);
        return executor != null ? executor : createExecutor (instructionName);
    }

    private Executor createExecutor (String instructionName) {
        int position = instructionName.lastIndexOf ('.');
        if (position < 0) {
            return findMethodAndCreateExecutor (instructionName);
        } else {
            String instructionSetName  = instructionName.substring (0, position);
            String realInstructionName = instructionName.substring (position + 1);
            return getInstructionSet (instructionSetName).getExecutor (realInstructionName);
        }
    }

    private InstructionSet getInstructionSet (String instructionSetName) {
        InstructionSet instructionSet = mInstructionSets.get (instructionSetName);
        if (instructionSet == null) {
            throw new ExecutionException ("unknown instruction set '%s'", instructionSetName);
        } else {
            return instructionSet;
        }
    }

    private Executor findMethodAndCreateExecutor (String instructionName) {
        Executor executor = null;
        for (InstructionSet set : mInstructionSets.values ()) {
            Executor newExecutor = set.getExecutor (instructionName);
            if (executor == null) {
                executor = newExecutor;
            } else if (newExecutor != null) {
                throw new ExecutionException ("more than one implementation of '%s'", instructionName);
            }
        }
        if (executor != null) {
            mExecutorCache.put (instructionName, executor);
            return executor;
        } else {
                throw new ExecutionException ("unknown instruction '%s'", instructionName);
        }
    }

    void cleanup () {
        for (InstructionSet instructionSet : mInstructionSets.values ()) {
            instructionSet.cleanup ();
        }
    }
}
