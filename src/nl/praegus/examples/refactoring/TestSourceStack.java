package nl.praegus.examples.refactoring;


public interface TestSourceStack {
    Scope getGlobalScope ();
    Scope getCurrentScope ();

    Symbol getSymbol (String name);
    void setValue (String name, String value);
    void copyStructure (String target, String source);
    void clearStructure (String name);
    
    boolean enterTestCase (String name, String description);
    void leaveTestCase ();
    void abortTestCase ();

    void abortTest ();

    void initAndPush (TestSource source);
    TestLine getTestLine ();
}
