package nl.praegus.examples.refactoring;

public class ProcedureException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private final Procedure mProcedure;


    public ProcedureException (Procedure procedure) {
        super ();
        mProcedure = procedure;
    }


    public Procedure getProcedure () {
        return mProcedure;
    }
}
