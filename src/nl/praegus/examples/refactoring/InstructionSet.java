package nl.praegus.examples.refactoring;


/**
 * An instruction set contains instruction implementations.
 * It retrieves an executor for an implemented instruction.
 */
public interface InstructionSet {
    /**
     * the name of the instruction set
     * @return the instruction set name
     */
    String getName ();

    /**
     * prepares the instruction set
     */
    void setup ();

    /**
     * retrieves an executor for an instruction (if is implemented once)
     * @param   instructionName the instruction name
     * @return  an executor for the instruction (if is implemented once)
     */
    Executor getExecutor (String instructionName);

    /**
     * cleans up the instruction set when it is no longer needed
     */
    void cleanup ();
}
