package nl.praegus.examples.refactoring;


public interface Executor {
    boolean execute (TestLine testLine);
}
