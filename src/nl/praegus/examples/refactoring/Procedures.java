package nl.praegus.examples.refactoring;

import java.util.HashMap;
import java.util.Map;


/**
 * The Procedures class is used to collect scripted instructions.
 */
public final class Procedures implements InstructionSet {
    private static final String INVALID_INSTRUCTION_SET_NAME = "something that will not match an instruction set name";

    private final Map<String, Procedure> mProcedures;
    private final ProcedureRunner mRunner;
    private final TestRunResultPublisher mPublisher;


    public Procedures (ProcedureRunner runner, TestRunResultPublisher publisher) {
        mProcedures = new HashMap<String, Procedure> ();
        mRunner     = runner;
        mPublisher  = publisher;
    }


    public void add (Procedure procedure) {
        mProcedures.put (procedure.getName (), procedure);
    }

    @Override
    public String getName () {
        return INVALID_INSTRUCTION_SET_NAME;
    }

    @Override
    public void setup () {
        // empty
    }

    @Override
    public Executor getExecutor (String instructionName) {
        return null;
    }

    @Override
    public void cleanup () {
        // empty
    }
}
