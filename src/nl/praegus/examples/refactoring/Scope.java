package nl.praegus.examples.refactoring;


public interface Scope {
    Scope createChildScope ();

    Scope getParent ();

    Symbol get (String name);
    Symbol getSymbol (String name);

    Symbol createConstant (String name, String value);
    Symbol createParameter (String name, String value);
    Symbol createVariable (String name, String value);
    Symbol createStructure (String name);
    Symbol createNumberSequence (String name, int value);
    Symbol createStringSequence (String name);
    Symbol createRepeatingStringSequence (String name);
}
