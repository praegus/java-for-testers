package nl.praegus.examples.refactoring;

import java.util.List;


// A Procedure is a scripted instruction.
public interface Procedure {
    public String getName ();
    public void addParameter (String name, boolean isOutput);
    public void addTable (List<List<String>> table);
}
