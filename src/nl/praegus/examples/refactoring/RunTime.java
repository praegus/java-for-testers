package nl.praegus.examples.refactoring;


/**
 * The runtime provides all engine functionality that an instruction may need.
 * It supports reporting errors and other execution information and
 * getting the local and global scope for getting and setting symbols.
 * <BR/>
 * It enables entering and leaving a test case.
 * <BR/>
 * It also allows an instruction (set) to create or get a shared object,
 * so data or logic can be shared between instruction sets.
 */
public interface RunTime {
    /**
     * Retrieves the event publisher.
     * @return  the publisher
     */
    TestRunResultPublisher getPublisher ();

    /**
     * Evaluates a PowerTools expression
     * @param   expression  the expression to evaluate
     * @return  the value of the expression
     */
    String evaluateExpression (String expression);

    /**
     * Retrieves the event publisher.
     * The report... methods should normally be used to report events.
     * @return  the publisher
     */
    TestSourceStack getSourceStack ();


    /**
     * Makes an object available to other instruction sets
     * @param   name    the name of the object
     * @param   object  the object to share
     * @return  false if an object of the specified name already exists, true otherwise
     */
    boolean addSharedObject (String name, Object object);

    /**
     * Returns an object available to other instruction sets
     * @param   name    the name of the object to get
     * @return  the specified shared object
     */
    Object getSharedObject (String name);
}
