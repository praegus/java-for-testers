package nl.praegus.examples.refactoring;


/**
 * The test line represents a line of data from a test source.
 * Its contents does not have to correspond to the data in the file,
 * as the test source may adjust it,
 * for instance to create an instruction name from several parts.<BR/>
 */
public interface TestLine {
    int getNrOfParts ();
    String getOriginalPart (int partNr);
    String getPart (int partNr);
    boolean isEmpty ();
}
